#!/usr/bin/env python2.7

import string
import sys
import hashlib
import random
import getopt
import itertools

# Constants
ALPHABET = string.ascii_lowercase + string.digits
LENGTH = 8
HASHES = 'hashes.txt'
PREFIX = ''

# Utility Function
def error(message, exit_code=1):
    print >>sys.stderr, message
    sys.exit(exit_code)

def usage(exit_code=0):
    error('''Usage: hulk.py [-a ALPHABET -l LENGTH -s HASHES -p PREFIX]
Options:
       -a  ALPHABET    Alphabet used for passwords
       -l  LENGTH      Length for passwords
       -s  HASHES      Path to file containing hashes
       -p  PREFIX      Prefix to use for each candidate password''', exit_code)


def md5sum(s):
    return hashlib.md5(s).hexdigest()

#Parsing command line options
try: 
    options, arguments = getopt.getopt(sys.argv[1:], "a:l:s:p:")
except getopt.GetoptError as e:
    print e
    usage(1)
for opt, arg in options:
    if opt == '-a':
        ALPHABET = arg
    elif opt == '-l':
        LENGTH = int(arg)
    elif opt == '-s':
        HASHES = arg
    elif opt == '-p':
        PREFIX = arg
    
# Main Execution

if __name__ == '__main__':
    hashes = set([l.strip() for l in open(HASHES)])
    #found = set()
    print 'in hulk!'
    for candidate in itertools.product(ALPHABET, repeat=LENGTH):
        candidate = PREFIX + ''.join(candidate) #joins tuple
        checksum = md5sum(candidate)
        if checksum in hashes:
            print candidate
