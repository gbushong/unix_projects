Project 02: Distributed Computing
=================================
1. hulk.py first reads in the hashes from hashes.txt into a set. It then uses itertools.product, which functions as a for loop, to read in varying letters of the alphabet for a certain length, then runs this string through the md5sum function. The md5sum function computes hte md5 hash of a string with the hashlib library, and if the string matches one in the hashlib library, it is printed.

	Hulk.py was tested against the code we created in class - it easily printed the output for up through length of 4. After that, the output took a bit longer to print. 

2. Fury.py utilizied hulk.py to crack passwords by running hulk to find passwords of a specified length that would increment. 
	Fury.py divides up the work among the different workers by ensuring that different workers are cracking different passwords, and that the same password has not been cracked. 
	Hulk.py is first called for each password length 0-6. it is called using length 6 and a prefix (of length 1 or 2 for password of length 7 and 8, respectively)
	Fury.py recovers from failures by getting rid of workers that have died (which it finds by checking on the status of its workers). It also keeps a journal to recover from failures.
	We tested fury.py by running ./fury.py, calling condor queue and ensuring that we had workers. We then ran the work queue test script to ensure that it was working.
		
3. Based on this project and discrete math, it would be more difficult to brute force passwords if they are longer. The fact that it did not take very long to crack passwords of length 1-4 or even of length 5 and then it took much, much longer to crack passwords of length 6 and even longer for lengths 7 and 8 makes us believe that the length is what makes passwords more difficult to crack.