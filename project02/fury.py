#!/usr/bin/env python2.7

import sys
import work_queue
import json
import os
import string
import itertools

# Constants

#LENGTH = 4
#TASKS = 200
HASHES = 'hashes.txt'
SOURCES = ('hulk.py', HASHES)
#PORT = 9067
JOURNALPATH = 'journal.json'
ALPHABET = string.ascii_lowercase + string.digits
JOURNAL = {"":""}
'''
def doit(command):
	if command in JOURNAL:
		print >>sys.stderr, 'Already did', command
	global TASK
	TASK = work_queue.Task(command)
	
	for source in SOURCES:
		TASK.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

	queue.submit(TASK)
'''
# Main Execution

if __name__ == '__main__':
    queue = work_queue.WorkQueue(work_queue.WORK_QUEUE_RANDOM_PORT, name='hulk-eturley', catalog=True)
# Catalog allows you to connect workers to master
    queue.specify_log('fury.log')

    try:
	JOURNAL = json.load(open(JOURNALPATH))
    except:
	JOURNAL = {}

    #for _ in range(TASKS):
    for LENGTH in range(1,8):
	if LENGTH > 5:
		for word in itertools.product(ALPHABET, repeat=LENGTH - 5):
			command = './hulk.py -l {} -s {} -p {}'.format(6, HASHES, ''.join(word))
			#doit(command)
                        if command in JOURNAL:
                            print >>sys.stderr, 'Already did', command
                        else:
                            TASK = work_queue.Task(command)

                            for source in SOURCES:
                                TASK.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

                            queue.submit(TASK)
	else:
        	command = './hulk.py -l {} -s {}'.format(LENGTH, HASHES)
                print command
		if command in JOURNAL:
                    print >>sys.stderr, 'Already did', command
        	else:
                    print 'erin'
                    TASK = work_queue.Task(command)

                    for source in SOURCES:
                        TASK.specify_file(source, source, work_queue.WORK_QUEUE_INPUT)

                    queue.submit(TASK)
        # this assumes that the friend has hulk. must tell work queue
        # that you want to run this, but you have to transfer file

    # Until the Master queue is empty
    while not queue.empty():
	# Wait for a task to complete
        TASK = queue.wait()

        if TASK and TASK.return_status == 0:
            # Write output of task to stdout
            JOURNAL[TASK.command] = TASK.output.split()
            with open('journal.json.new', 'w') as stream:
                json.dump(JOURNAL, stream)
            os.rename('journal.json.new', 'journal.json')
            sys.stdout.write(TASK.output)
            sys.stdout.flush()
