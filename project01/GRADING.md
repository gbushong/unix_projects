Project 01 - Grading
====================

**Score**: 16.75 / 20

Deductions
----------

* Thor

    - 0.25  Does not handle command line arguments (-r, -p)
    - 1.0   Not fully concurrent (fork all the things, then wait for all processes)
    - 0.5   Child needs to exit after all requests
    - 0.25  Does not perform all PROCESSES

* Spidey

    - 0.5   Failed handling static file and CGI script
    - 0.25  Missing error status code
    *       Child needs to exit after request is complete
    *       Should use os.popen, not subprocess.Popen

* Report

    - 0.25  Missing Makefile
    - 0.25  Insufficient analysis
    *       Plot is off-centered

Comments
--------
