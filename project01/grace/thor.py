#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import sys
import time

# Constants

ADDRESS  = 'www.example.com'
PORT     = 80
PROGRAM  = os.path.basename(sys.argv[0])
LOGLEVEL = logging.INFO
REQUESTS = 1
PROCESSES = 1

# Utility Functions

def usage(exit_code=0):
    print >>sys.stderr, '''Usage: {} [-r REQUESTS -p PROCESSES -v] URL

Options:

    -h              Show this help message
    -v              Set logging to DEBUG level

    -r REQUESTS     Number of requests per process (default is 1)
    -p PROCESSES    Number of processes (default is 1)
'''.format(PROGRAM)
    sys.exit(exit_code)

def wait_fd(child):
    try:
        global STAT
        PID, STAT = os.wait()
    except OSError as e:
        error(e)

def fork_fd():
    try:
        global PID
        PID = os.fork()
    except OSError as e:
        print >>sys.stderr, 'Unable to fork: {}'.format(e)
        sys.exit(1)
# TCPClient Class

class TCPClient(object):
    def __init__(self, address=ADDRESS, port=PORT):
        ''' Construct TCPClient object with the specified address and port '''
        self.logger  = logging.getLogger()                              # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = ADDRESS
        self.port    = PORT                                             # Store port to lisen on

    def handle(self):
        ''' Handle connection '''
        self.logger.debug('Handle')
        raise NotImplementedError

    def run(self):
        ''' Run client by connecting to specified address and port and then
        executing the handle method '''
        try:
            # Connect to server with specified address and port, create file object
            self.socket.connect((self.address, self.port))
            self.stream = self.socket.makefile('w+')
    
        except socket.error as e:
            self.logger.error('Could not connect to {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.debug('Connected to {}:{}...'.format(self.address, self.port))

        # Run handle method and then the finish method
        try:
            self.handle()
        except Exception as e:
            self.logger.exception('Exception: {}'.format(e))
        finally:
            self.finish()

    def finish(self):
        ''' Finish connection '''
        self.logger.debug('Finish')
        try:
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# HTTPClient Class

class HTTPClient(TCPClient):
    def __init__(self, url, port):

        self.logger = logging.getLogger()
        self.logger.debug('URL: {}'.format(url))

        # Parsing URL
        self.url = url.split('://')[-1]
        
        # Get Path
        if '/' not in self.url:
            self.path = '/'
        else:
            x = self.url.split('?',1)
            self.path = '/' + x[0].split('/', 1)[-1]

        # Get Port
        if ':' not in self.url:
            self.port = PORT
        else:
            x = self.url.split('/', 1)
            self.port = x[0].split(':', 1)[-1]    

        self.logger.debug('PATH: {}'.format(self.path))
        self.logger.debug('PORT: {}'.format(self.port))

        # Get address
        x = self.url.split('/', 1)
        y = x[0].split(':', 1)
        self.domain = y[0]

        # Convert domain name to ip address if needed
        try:
            self.address = gethostbyname(self.domain)
        except:
            self.address = self.domain

        self.logger.debug('ADDRESS: {}'.format(self.address))

        # Get host
        self.host = socket.gethostname()
        self.address = ADDRESS

        TCPClient.__init__(self, self.address, int(self.port))

    def handle(self):
        ''' Handle connection by reading data and then writing it back until EOF '''
        self.logger.debug('Handle')
        try:
            self.logger.debug('Sending request...')
            self.stream.write('GET {} HTTP/1.0\r\n'.format(self.path))
            self.stream.write('Host: {}\r\n'.format(self.host))
            self.stream.write('\r\n')
            self.stream.flush()

        except socket.error:
            pass
        try:
           data = self.stream.readline()
           while data:
               # Read from Server to STDOUT
               print data

               # Read from STDIN
               data = self.stream.readline()
        except socket.error:
           pass    # Ignore socket errors

# Main Execution

if __name__ == '__main__':
    # Parse command-line arguments
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hv")
    except getopt.GetoptError as e:
        usage(1)

    for option, value in options:
        if option == '-v':
            LOGLEVEL = logging.DEBUG
        elif option == '-r':
            REQUESTS = value
        elif option == '-p':
            PROCESSES = value
        else:
            usage(1)

    if len(arguments) >= 1:
        ADDRESS = arguments[0]
    elif len(arguments) >= 2:
        PORT    = int(arguments[1])
    else:
        usage(1)

    # Set logging level
    logging.basicConfig(
        level   = LOGLEVEL,
        format  = '[%(asctime)s] %(message)s',
        datefmt = '%Y-%m-%d %H:%M:%S',
    )

    runtime = 0
    for process in range(int(PROCESSES)):
        fork_fd()

        if PID == 0: # Child
            for request in range(int(REQUESTS)):
                start = time.time()
                client = HTTPClient(ADDRESS, PORT)
                try:
                    client.run()
                    end = time.time()
                except KeyboardInterrupt:
                    sys.exit(0)
                logging.debug('Elapsed time: {0:.2f} seconds.'.format(end - start))
                runtime += (end - start)
                logging.debug('Average Elapsed time: {0:.2f} seconds.'.format(runtime/(process+1)))
        else:   # Parent
            wait_fd(PID)
            logging.debug('Process {} terminated with exit status {}'.format(PID, STAT))
            sys.exit(STAT)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
