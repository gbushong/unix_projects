#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import signal
import sys
import mimetypes

# Constants

ADDRESS  = '0.0.0.0'
PORT     = 9234
BACKLOG  = 0
LOGLEVEL = logging.INFO
PROGRAM  = os.path.basename(sys.argv[0])
DOCROOT = '.'
FORKING = False

# Utility Functions

def usage(exit_code=0):
    print >>sys.stderr, '''Usage: {program} [-d DOCROOT -p PORT -f -v]

Options:

    -h           Show this help message
    -f           Enable forking mode
    -v           Set logging to DEBUG level
    
    -p PORT      TCP Port to listen to (default is {port})
    -d DOCROOT   Handles HTTP requests for files in given directory
'''.format(port=PORT, program=PROGRAM)
    sys.exit(exit_code)

def fork_fd():
    try:
        global PID
        PID = os.fork()
    except OSError as e:
        print >>sys.stderr, 'Unable to fork: {}'.format(e)
        sys.exit(1)

# BaseHandler Class
class BaseHandler(object):
    def __init__(self, fd, address):
        ''' Construct handler from file descriptor and remote client address '''
        self.logger  = logging.getLogger()        # Grab logging instance
        self.socket  = fd                         # Store socket file descriptor
        self.address = '{}:{}'.format(*address)   # Store address
        self.stream  = self.socket.makefile('w+') # Open file object from file descriptor
        self.debug('Connect')

    def debug(self, message, *args):
        ''' Convenience debugging function '''
        message = message.format(*args)
        self.logger.debug('{} | {}'.format(self.address, message))

    def info(self, message, *args):
        ''' Convenience information function '''
        message = message.format(*args)
        self.logger.info('{} | {}'.format(self.address, message))

    def warn(self, message, *args):
        ''' Convenience warning function '''
        message = message.format(*args)
        self.logger.warn('{} | {}'.format(self.address, message))

    def error(self, message, *args):
        ''' Convenience error function '''
        message = message.format(*args)
        self.logger.error('{} | {}'.format(self.address, message))

    def exception(self, message, *args):
        ''' Convenience exception function '''
        message = message.format(*args)
        self.logger.exception('{} | {}'.format(self.address, message))

    def handle(self):
        ''' Handle connection '''
        self.debug('Handle')
        raise NotImplementedError

    def finish(self):
        ''' Finish connection by flushing stream, shutting down socket, and
        then closing it '''
        self.debug('Finish')
        try:
            self.stream.flush()
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error as e:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# HTTPHandler Class
class HTTPHandler(BaseHandler):
    def __init__(self, fd, address, docroot=DOCROOT):
        self.docroot = os.path.abspath(DOCROOT)
        BaseHandler.__init__(self, fd, address)

    def handle(self):
        ''' Handle connection by reading data and then writing it back until EOF '''
        self.debug('Handle')
        self._parse_request()
        self.uripath = self.docroot + os.environ['REQUEST_URI']
        self.logger.debug(self.docroot)
        self.logger.debug(self.uripath)
        self.logger.debug(os.path.exists(self.uripath))
        
        if not os.path.exists(self.uripath) or self.docroot not in self.uripath:
            self._handle_error(404) #error code 404 - does not exist
        elif os.path.isfile(self.uripath) and os.access(self.uripath, os.R_OK):
            self._handle_file()
        elif os.path.isfile(self.uripath) and os.access(self.uripath, os.X_OK):
            self._handle_script()
        elif os.path.isdir(self.uripath) and os.access(self.uripath, os.R_OK):
            self._handle_directory()
        else:
            self._handle_error(403) #error code 403 - no permission

    def _parse_request(self):
        os.environ['REMOTE_ADDR']=self.address.split(':', 1)[0]
        os.environ['REMOTE_HOST']=self.address.split(':', 1)[0]
        os.environ['REMOTE_PORT']=self.address.split(':', 1)[-1]
        data = self.stream.readline().strip()
        cond = True
        while data:
            if cond:
                os.environ['REQUEST_METHOD']=data.split()[0]
                resource=data.split()[1]
                os.environ['REQUEST_URI']=resource.split('?')[0]
            if len(resource.split('?'))>=2:
                os.environ['QUERY_STRING']=resource.split('?')[1]
            else:
                data = data.split(': ', 1)
                if len(data)>=2:
                    os.environ['HTTP_' + data[0].upper().replace('-', '_')]=data[1]
            cond = False
            data = self.stream.readline().strip()

    def _handle_file(self):
        self.logger.debug(self.uripath)
        mimetype, _ = mimetypes.guess_type(self.uripath)
        if mimetype is None:
            mimetype= 'application/octet-stream'
        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-Type: {}\r\n'.format(mimetype))
        for line in open(self.uripath, 'rb'):
            self.stream.write(line)
        self.stream.flush()

    def _handle_directory(self):
        directory=self.uripath
        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-Type: text/html\r\n')
        self.stream.write('\r\n')
        self.stream.write('''<html>
            <head>
                <title> {} </title>
            </head>
            <body>'''.format(DOCROOT))
        for item in os.listdir(directory):
            path = os.path.join(directory, item)
            if os.path.isfile(path):
                itemType = 'File'
            elif os.path.isdir(path):
                itemType = 'Directory'
            self.stream.write('<p><b>Item: </b>' + '<a href="' + os.path.join(os.environ['REQUEST_URI'], item) + '">' + item + '</a>' + '&nbsp&nbsp&nbsp<b>Size: </b>' + str(os.path.getsize(path)) + '\t<b>Type: </b>' + itemType + '</p>')

        self.stream.write('''
              </body>
              </html>
              ''')
        self.stream.flush()

    def _handle_script(self):
        signal.signal(signal.SIGCHLD, signal.SIG_DFL)
#        for line in os.popen(self.uripath):
#            self.stream.write(line)
#        signal.signal(signal.SIGCHLD, signal.SIG_IGN)
        pipe = subprocess.Popen(self.uripath, stdout=subprocess.PIPE)
        out = pipe.communicate()[0]
        self.stream.write(out)
        signal.signal(signal.SIGCHLD, signal.SIG_IGN)

    def _handle_error(self, codeNum):
        self.stream.write('HTTP/1.0 200 OK\r\n')
        self.stream.write('Content-Type: text/html\r\n')
        self.stream.write('\r\n')

        if codeNum == 404:
            self.stream.write('''
                <!DOCTYPE html>
                <html>
                <body>
                <h2 align="center"> Error 404: Not Found </h2>
                <center>
                <a href="https://www.youtube.com/watch?v=oabcM9SOF-E">Sad</a></center>
                </body>
                </html>''')
            self.stream.flush()
        else:
            self.stream.write('''
                <!DOCTYPE html>
                <html>
                <body>
                <h2 align="center"> Error 403: Access Denied </h2>
                <center><a href="https://www.youtube.com/watch?v=yHJeqnU_rtE">Sad</a></center>
                </body>
                </html>''')
            self.stream.flush()

# TCPServer Class

class TCPServer(object):

    def __init__(self, address=ADDRESS, port=PORT, handler=HTTPHandler):
        ''' Construct TCPServer object with the specified address, port, and
        handler '''
        self.logger  = logging.getLogger()                              # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address                                          # Store address to listen on
        self.port    = port                                             # Store port to lisen on
        self.handler = handler                                          # Store handler for incoming connections

    def run(self):
        ''' Run TCP Server on specified address and port by calling the
        specified handler on each incoming connection '''
        try:
            # Bind socket to address and port and then listen
            self.socket.bind((self.address, self.port))
            self.socket.listen(BACKLOG)
        except socket.error as e:
            self.logger.error('Could not listen on {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.info('Listening on {}:{}...'.format(self.address, self.port))

        while True:
            # Accept incoming connection
            client, address = self.socket.accept()
            self.logger.debug('Accepted connection from {}:{}'.format(*address))

            # Instantiate handler, handle connection, finish connection
            if not FORKING:
                try:
                    handler = self.handler(client, address)
                    handler.handle()
                except Exception as e:
                    handler.exception('Exception: {}', e)
                finally:
                    handler.finish()
            else:
                fork_fd()
                if PID == 0:    # child
                    try:
                        handler = self.handler(client, address)
                        handler.handle()
                    except Exception as e:
                        handler.exception('Exception {}', e)
                    finally:
                        handler.finish()
                else:       # parent
                    client.close()
# Main Execution

if __name__ == '__main__':
    # Parse command-line arguments
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hp:vfd:")
    except getopt.GetoptError as e:
        usage(1)

    for option, value in options:
        if option == '-p':
            PORT = int(value)
        elif option == '-v':
            LOGLEVEL = logging.DEBUG
        elif option == '-f':
            FORKING = True
        elif option == '-d':
            DOCROOT = value
        elif option == '-p':
            PORT = int(value)
        else:
            usage(1)

    # Set logging level
    logging.basicConfig(
        level   = LOGLEVEL,
        format  = '[%(asctime)s] %(message)s',
        datefmt = '%Y-%m-%d %H:%M:%S',
    )

    # Instantiate and run server
    server = TCPServer(ADDRESS, PORT, HTTPHandler)

    try:
        server.run()
    except KeyboardInterrupt:
        sys.exit(0)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
