CSE-20189-SP16 - Projects
=========================

This is the group repository for the final two projects in [CSE-20189-SP16].

Group Members
-------------

- Erin Turley         (eturley@nd.edu)
- Caroline Braun      (cbraun1@nd.edu)
- Grace Bushong       (gbushong@nd.edu)

[CSE-20189-SP16]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/
